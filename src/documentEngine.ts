import { DocumentEngine } from "@design-experiences/packaging/documentEngine";
import { cimDoc, designRequirements } from "./sku";
// import { cimDoc } from "./cimdoc";
// import { designRequirements } from "./requirement";
import { authConfig } from "./auth";
import { createPackagingDocumentEngine } from "@design-experiences/packaging/state";

export const documentEngine =
  cimDoc &&
  designRequirements &&
  createPackagingDocumentEngine({
    cimDoc,
    designRequirements,
    authConfig,
  });
