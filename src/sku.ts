import { getSurfaceUrl } from "@design-stack/cimdoc-design-lib/productsSdk";
import {
  LoadingExperienceSpinner,
  useDocument,
} from "@design-stack/documentation-core";

const sku = "CIM-8TFAUTNP";
const skuVars = { Width: "30.48", Height: "25.4", Depth: "10.16" };

const surfaceUrl = getSurfaceUrl(sku, skuVars);

// eslint-disable-next-line react-hooks/rules-of-hooks
export const { cimDoc, designRequirements } = useDocument({
  surfaceUrl,
});
