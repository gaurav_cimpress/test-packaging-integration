import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

import { PackagingDesignExperience } from "@design-experiences/packaging";
import {
  DocumentEngine,
  // DocumentEngineProvider,
} from "@design-experiences/packaging/documentEngine";

import { cimDoc, designRequirements } from "./sku";
import { authConfig } from "./auth";
import { createPackagingDocumentEngine } from "@design-experiences/packaging/state";

import { getSurfaceUrl } from "@design-stack/cimdoc-design-lib/productsSdk";
import { useDocument } from "@design-stack/documentation-core";

const sku = "CIM-8TFAUTNP";
const skuVars = { Width: "30.48", Height: "25.4", Depth: "10.16" };
const surfaceUrl = getSurfaceUrl(sku, skuVars);

const previewConfigObj = {
  modelConfigUrl:
    "https://assets.documents.cimpress.io/v3/assets/41bf8c17-3c9b-4df6-920f-85e3e40f6d09/content",
};

function MyApp() {
  const { cimDoc, designRequirements } = useDocument({
    surfaceUrl,
  });

  const documentEngine =
    cimDoc &&
    designRequirements &&
    createPackagingDocumentEngine({
      cimDoc,
      designRequirements,
      authConfig,
    });

  return (
    // <DocumentEngineProvider documentEngine={documentEngine}>
    <div>
      {documentEngine && (
        <PackagingDesignExperience
          documentEngine={documentEngine}
          previewConfig={previewConfigObj}
        />
      )}
    </div>
    // </DocumentEngineProvider>
  );
}

// const element = (
//     <PackagingDesignExperience documentEngine={documentEngine as DocumentEngine} previewConfig={previewConfigObj}/>
// );
ReactDOM.render(<MyApp />, document.getElementById("root"));

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
