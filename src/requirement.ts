import { createPackagingDesignRequirements } from '@design-experiences/packaging/state';

export const designRequirements = createPackagingDesignRequirements({
    widthInMm: 279.4,
    heightInMm: 215.9,
}, 5, 10);
