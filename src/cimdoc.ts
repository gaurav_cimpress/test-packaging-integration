import { buildPackagingCimDoc } from "@design-experiences/packaging/state";
import { CimDoc } from "@design-stack/cimdoc-design-lib";
import { designRequirements } from "./requirement";

const fontRepositoryUrl = "";

export const cimDoc: CimDoc = buildPackagingCimDoc({
  designRequirements,
});
